﻿using System.Data.SqlClient;
using CaietPracticaV2.ApplicationServices;
using CaietPracticaV2.Enums;

namespace CaietPracticaV2.Model.DAL
{
    public class LoginCredentialsDAO
    {
        private LoginCredentialsDAO(){}

        public static bool IsValidCredentials(string email, string password)
        {
            bool isValid;
            MSSQLDatabaseConnection.Open();

            using (var command = new SqlCommand(@"SELECT COUNT(1) FROM utilizator 
                                                WHERE email='" + email + "' AND parola='" + password + "'",
                MSSQLDatabaseConnection.GetConnection()))
            {
                isValid = (int) command.ExecuteScalar()!=0;
            }

            MSSQLDatabaseConnection.Close();

            return isValid;
        }

        internal static UserType TypeOfLoggedUser(string email)
        {
            UserType usertype;
            MSSQLDatabaseConnection.Open();

            using (var command = new SqlCommand(@"SELECT rol 
                                                FROM utilizatori 
                                                WHERE email='" + email + "'",
                MSSQLDatabaseConnection.GetConnection()))
            {
                var dataReader = command.ExecuteReader();
                dataReader.Read();
                usertype = dataReader["rol"].ToString() == "indrumator" ? UserType.Indrumator : UserType.Student;
                dataReader.Close();
            }

            MSSQLDatabaseConnection.Close();

            return usertype;
        }
    }
}