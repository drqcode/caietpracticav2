﻿using System;
using CaietPracticaV2.Enums;

namespace CaietPracticaV2.ApplicationServices
{
    class FormsNavigator:IFormsNavigator
    {

        public void NavigateTo(FormType form)
        {
            switch (form)
            {
                case FormType.LoginForm:
                    break;
                default:
                    throw new ArgumentOutOfRangeException("form");
            }
        }
    }
}
