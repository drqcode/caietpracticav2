﻿using System;
using CaietPracticaV2.Enums;

namespace CaietPracticaV2.ApplicationServices
{
    public class ApplicationController
    {
        private static IFormsNavigator _navigator;

        internal static void Register(FormsNavigator navigator)
        {
            _navigator = navigator;
        }
        internal static void NavigateTo(FormType formType)
        {
            if (_navigator == null)
            {
                throw new ArgumentNullException();
            }
            _navigator.NavigateTo(formType);
        }
    }
}