﻿using CaietPracticaV2.Enums;

namespace CaietPracticaV2.ApplicationServices
{
    public interface IFormsNavigator
    {
        void NavigateTo(FormType form);
    }
}