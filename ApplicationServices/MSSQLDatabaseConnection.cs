﻿using System;
using System.Data.SqlClient;

namespace CaietPracticaV2.ApplicationServices
{
    public class MSSQLDatabaseConnection
    {
        private static MSSQLDatabaseConnection _instance;

        private static SqlConnection _sqlConnection;

        private MSSQLDatabaseConnection()
        {
            _sqlConnection = new SqlConnection("Data Source=.;Initial Catalog=app1;Integrated Security=True");
        }

        public static void CreateConnection()
        {
            if (_instance == null)
            {
                _instance = new MSSQLDatabaseConnection();
            }
        }
        public static void Open()
        {
            if (_sqlConnection == null)
            {
                throw new ArgumentNullException();
            }
            _sqlConnection.Open();
        }
        public static void Close()
        {
            if (_sqlConnection == null)
            {
                throw new ArgumentNullException();
            }
            _sqlConnection.Close();
        }

        public static SqlConnection GetConnection()
        {
            return _sqlConnection;
        }
    }
}