﻿using System.Windows.Forms;
using CaietPracticaV2.ApplicationServices;
using CaietPracticaV2.Enums;

namespace CaietPracticaV2
{
    public class CaietPracticaApplicationContext:ApplicationContext
    {
         public CaietPracticaApplicationContext()
        {
            var navigator = new FormsNavigator();
            ApplicationController.Register(navigator);
            ApplicationController.NavigateTo(FormType.LoginForm);
        }
    }

   
}